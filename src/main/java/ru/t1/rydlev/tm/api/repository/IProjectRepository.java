package ru.t1.rydlev.tm.api.repository;

import ru.t1.rydlev.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}
