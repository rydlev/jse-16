package ru.t1.rydlev.tm.command.project;

import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().removeByIndex(index);
    }

    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

}
