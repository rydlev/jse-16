package ru.t1.rydlev.tm.command.task;

import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(id);
        showTask(task);
    }

    @Override
    public String getDescription() {
        return "Display task by id.";
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

}
