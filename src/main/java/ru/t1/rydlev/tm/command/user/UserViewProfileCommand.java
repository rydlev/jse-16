package ru.t1.rydlev.tm.command.user;

import ru.t1.rydlev.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @Override
    public String getDescription() {
        return "View profile of current user.";
    }

    @Override
    public String getName() {
        return "view-user-profile";
    }

}
