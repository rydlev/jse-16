package ru.t1.rydlev.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        getProjectTaskService().removeProjects();
    }

    @Override
    public String getDescription() {
        return "Clear projects.";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

}
